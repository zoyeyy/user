package main

import (
	"fmt"
)

func main() {
	//var sto = 123
	//var sto1 = "aab"
	//var endtime = "2021-1-1"
	//var url = "Code=%d&endtime=%s---sto1-%p"
	//var a int = random()
	//var tar_url = fmt.Sprintf(url, sto, endtime, sto1)
	//var aa, bb int
	//aa = 1
	//bb = 2
	//var cc string
	//aa, bb = bb, aa
	const WIDTH = 5
	const LENGTH = 10
	var area int = WIDTH * LENGTH
	const a, b, c = 1, false, "ss"
	const (
		unknown = 0
		female  = 1
		male    = 2
	)
	fmt.Println("面积为:%d", area, a, b, c, unknown, female, male)
}
